package main

import (
	pb "bitbucket.org/goautohome/go-autohome-proto"
	"fmt"
	"testing"
	"time"
)

func TestPioneerCaller_Writer(t *testing.T) {
	caller, err := NewPioneerCaller("192.168.178.28", 8102)
	if err != nil {
		panic(err)
	}
	caller.StartListen()
	caller.Send("PO")
	time.Sleep(time.Second * 2)
}
func TestPioneerPlugin_SetConfig(t *testing.T) {
	pl := GetAutohomePlugin()
	config := "{\"devices\":[{\"ip\":\"192.168.178.28\",\"port\":8102,\"name\":\"Living Room\",\"uniqid\":\"asdftese\"}]}"
	err := pl.SetConfig(config)
	if err != nil {
		panic(err)
	}
}
func TestPioneerPlugin_Init(t *testing.T) {
	pl := GetAutohomePlugin()

	config := "{\"devices\":[{\"ip\":\"192.168.178.28\",\"port\":8102,\"name\":\"Living Room\",\"uniqid\":\"asdftese\"}]}"
	pl.SetConfig(config)

	pl.OnAdd(func(device *pb.Device) {
		fmt.Println(device)
	})
	pl.OnUpdate(func(device *pb.Device, field *pb.TraitField) {
		fmt.Println(device.HWID, field.FieldName, field.Value)
	})
	devs := pb.Devices{}
	pl.Init(devs)

	time.Sleep(time.Second * 3)
}

func TestPioneerPlugin_DevicePower(t *testing.T) {
	pl := GetAutohomePlugin()

	config := "{\"devices\":[{\"ip\":\"192.168.178.28\",\"port\":8102,\"name\":\"Living Room\",\"uniqid\":\"asdftese\"}]}"
	pl.SetConfig(config)
	pl.OnAdd(func(device *pb.Device) {
		for _, t := range device.Traits {
			if t.TraitName == "power" {
				t.Fields["set"].Value = "1"
				err := pl.SetState(device, t)
				if err != nil {
					panic(err)
				}
			}
		}
	})
	pl.OnUpdate(func(device *pb.Device, field *pb.TraitField) {
		fmt.Printf("Device '%s' has new val on '%s': %s", device.HWID, field.FieldName, field.Value)
	})
	devs := pb.Devices{}
	pl.Init(devs)

	time.Sleep(time.Second * 6)
}

func TestPioneerPlugin_Vol(t *testing.T) {
	pl := GetAutohomePlugin()

	config := "{\"devices\":[{\"ip\":\"192.168.178.28\",\"port\":8102,\"name\":\"Living Room\",\"uniqid\":\"asdftese\"}]}"
	pl.SetConfig(config)
	pl.OnAdd(func(device *pb.Device) {
		for _, t := range device.Traits {
			if t.TraitName == "volume" {
				t.Fields["set"].Value = "30"
				err := pl.SetState(device, t)
				if err != nil {
					panic(err)
				}
			}
		}
	})
	pl.OnUpdate(func(device *pb.Device, field *pb.TraitField) {
		fmt.Printf("Device '%s' has new val on '%s': %s \n", device.HWID, field.FieldName, field.Value)
	})
	devs := pb.Devices{}
	pl.Init(devs)

	time.Sleep(time.Second * 7)
}
func TestPioneerPlugin_UpdateListener(t *testing.T) {
	pl := GetAutohomePlugin()

	config := "{\"devices\":[{\"ip\":\"192.168.178.28\",\"port\":8102,\"name\":\"Living Room\",\"uniqid\":\"asdftese\"}]}"
	pl.SetConfig(config)
	pl.OnUpdate(func(device *pb.Device, field *pb.TraitField) {
		fmt.Printf("Device '%s' has new val on '%s': %s \n", device.HWID, field.FieldName, field.Value)
	})
	devs := pb.Devices{}
	pl.Init(devs)

	time.Sleep(time.Second * 6)
}
