# Pioneer-AV-Receiver plugin for [Go-Autohome](https://bitbucket.org/goautohome/go-autohome-daemon)

# build
go build -buildmode=plugin -o ./pioneer.so

# config:
``
{"devices":[{
    "ip":"192.168.178.28",
    "port":8102,
    "name":"Living Room",
    "uniqid":"pioneerVSX915_wohnzimmer_home"}]}
``
