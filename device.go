package main

import (
	iplugin "bitbucket.org/goautohome/go-autohome-interface/plugin"
	pb "bitbucket.org/goautohome/go-autohome-proto"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

const MaxVol = 90 // 185
type PioneerDevice struct {
	IP          string
	Port        int
	Name        string
	Caller      *PioneerCaller
	ProtoDevice *pb.Device
	volupdate   chan int
	plDevice    iplugin.PlDevice
}

func GetDevice(device JsonDevice) *PioneerDevice {
	caller, err := NewPioneerCaller(device.Ip, device.Port)
	if err != nil {
		panic(err)
	}
	dev := iplugin.PlDevice{
		DeviceName: device.Name,
		DeviceHwid: device.Uniqid,
	}

	dev.Properties = append(
		dev.Properties,
		iplugin.PlProp{
			PropName:        "volume",
			PropDescription: "Volume of the receiver",
			PropUI:          "{slider:{min:0,max:100}}",
			PropCmds: []iplugin.PlCommands{
				{CmdName: "set", CmdRead: false, CmdWrite: true},
				{CmdName: "get", CmdRead: true, CmdWrite: false},
			},
		},
	)

	dev.Properties = append(
		dev.Properties,
		iplugin.PlProp{
			PropName:        "power",
			PropDescription: "Switch on and off",
			PropUI:          "{switch:{}}",
			PropCmds: []iplugin.PlCommands{
				{CmdName: "set", CmdRead: false, CmdWrite: true},
				{CmdName: "get", CmdRead: true, CmdWrite: false},
			},
		},
	)
	dev.Properties = append(
		dev.Properties,
		iplugin.PlProp{
			PropName:        "source",
			PropDescription: "Source select",
			PropUI:          "{select:{values:getSourceList}}",
			PropCmds: []iplugin.PlCommands{
				{CmdName: "set", CmdRead: false, CmdWrite: true},
				{CmdName: "get", CmdRead: true, CmdWrite: false},
				{CmdName: "getSourceList", CmdRead: true, CmdWrite: false},
			},
		},
	)
	return &PioneerDevice{
		IP:        device.Ip,
		Port:      device.Port,
		Caller:    caller,
		volupdate: make(chan int),
		plDevice:  dev}
}

func (d *PioneerDevice) GetPlDevice() *iplugin.PlDevice {
	return &d.plDevice
}
func (d *PioneerDevice) Start() error {
	d.Caller.StartListen()
	// Query All Inputs frequently
	go func() {
		d.Caller.Send("?V")
		d.Caller.Send("?P")
		d.Caller.Send("?M")
		d.Caller.Send("?F")
		time.Sleep(time.Second * 10)
	}()
	return nil
}

func (d *PioneerDevice) SetState(u *pb.Command) error {
	if !u.CmdWrite {
		return errors.New("Command no setable")
	}
	switch u.Property.PropName {
	case "volume":
		if u.CmdName != "set" {
			return errors.New("Unkown volume commmand")
		}
		val, err := strconv.Atoi(u.CmdValue)
		if err != nil {
			return err
		}
		getCmd := u.Property.GetCommandsByName()["get"]
		if getCmd == nil {
			return errors.New("No read value.")
		}
		go func() {
			var current int
			d.Caller.Send("?V")
			for {
				time.Sleep(time.Millisecond * 100)
				current, _ = strconv.Atoi(getCmd.CmdValue)
				if current > val {
					d.Caller.Send("VD")
				}
				if current < val {
					d.Caller.Send("VU")
				}
				if current == val {
					return
				}
			}
		}()
	case "source":

	case "power":
		if u.CmdName != "set" {
			return errors.New("Unkown volume commmand")
		}
		if u.CmdValue == "1" {
			d.Caller.Send("PO")
		} else {
			d.Caller.Send("PF")
		}
	}
	return nil
}
func (d *PioneerDevice) UpdateNotifier(update func(cmd *pb.Command)) {

	go func() {
		for {
			val := <-d.Caller.RecCommands
			switch true {
			case strings.Index(val, "VOL") == 0:
				vol := strings.Replace(val, "VOL", "", 1)
				f := d.ProtoDevice.GetPropertiesByName()["volume"].GetCommandsByName()["get"]
				if f == nil {
					fmt.Errorf("Field not found %s", "volume")
					return
				}
				volint, err := strconv.Atoi(vol)
				if err != nil {
					return
				}
				var volpercent float64
				volpercent = (float64(100) / float64(MaxVol)) * float64(volint)
				f.CmdValue = fmt.Sprintf("%d", int(volpercent))
				f.Device = d.ProtoDevice
				update(f)
			case strings.Index(val, "FN") == 2:
				f := d.ProtoDevice.GetPropertiesByName()["source"].GetCommandsByName()["get"]
				f.CmdValue = fmt.Sprintf("%d", val)
				f.Device = d.ProtoDevice
				update(f)
			case strings.Index(val, "PWR") == 0:
				f := d.ProtoDevice.GetPropertiesByName()["power"].GetCommandsByName()["get"]
				if val == "PWR1" {
					f.CmdValue = fmt.Sprintf("%d", "0")
				} else {
					f.CmdValue = fmt.Sprintf("%d", "1")
				}
				f.Device = d.ProtoDevice
				update(f)
			}
		}
	}()
}
