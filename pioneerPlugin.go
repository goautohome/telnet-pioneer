package main

import (
	iplugin "bitbucket.org/goautohome/go-autohome-interface/plugin"
	pb "bitbucket.org/goautohome/go-autohome-proto"
	"encoding/json"
)

const Version = "0.0.1"
const EngineName = "PioneerPlugin"
const Type = "device_hub"

type JsonDevice struct {
	Ip     string `json:"ip"`
	Port   int    `json:"port"`
	Name   string `json:"name"`
	Uniqid string `json:"uniqid"`
}
type PluginConf struct {
	Devices []JsonDevice `json:"devices"`
}
type PioneerPlugin struct {
	devices map[string]*PioneerDevice
	config  PluginConf

	AddDevice    func(*iplugin.PlDevice) (*pb.Device, error)
	RemoveDevice func(device *pb.Device)
	UpdateDevice func(*pb.Command)
}

func (s *PioneerPlugin) SetConfig(config string) error {
	err := json.Unmarshal([]byte(config), &s.config)
	for _, d := range s.config.Devices {
		s.devices[d.Uniqid] = GetDevice(d)
	}
	return err
}
func (s *PioneerPlugin) GetInfo() iplugin.PluginInfo {
	return iplugin.PluginInfo{Version: Version, EngineName: EngineName, Type: Type}
}
func (s *PioneerPlugin) Init(devs []*pb.Device) error {
PLDEVICELOOP:
	for hwid, pd := range s.devices {
		if len(devs) > 0 {
			for _, d := range devs {
				if d.DeviceHwid == hwid {
					pd.ProtoDevice = d
					break PLDEVICELOOP
				}
			}
		}
		if s.UpdateDevice != nil {
			pd.UpdateNotifier(s.UpdateDevice)
		}
		if s.AddDevice != nil {
			s.AddDevice(pd.GetPlDevice())
		}
		// start Telnet
		pd.Start()
	}
	return nil
}
func (s *PioneerPlugin) OnAdd(onAdd func(*iplugin.PlDevice) (*pb.Device, error)) {
	s.AddDevice = onAdd
}
func (s *PioneerPlugin) OnRemove(onRemove func(device *pb.Device)) {
	s.RemoveDevice = onRemove
}
func (s *PioneerPlugin) OnUpdate(onUpdate func(*pb.Command)) {
	s.UpdateDevice = onUpdate
}
func (s *PioneerPlugin) SetState(u *pb.Command) error {
	return s.devices[u.Device.DeviceHwid].SetState(u)
}

func (s *PioneerPlugin) Close() {

}
