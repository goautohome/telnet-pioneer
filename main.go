package main

import (
	iplugin "bitbucket.org/goautohome/go-autohome-interface/plugin"
	pb "bitbucket.org/goautohome/go-autohome-proto"
)

var instance PioneerPlugin

type FinalPlugin struct {
}

func (p FinalPlugin) SetConfig(config string) error {
	return instance.SetConfig(config)
}

// Return JSON with information
func (p FinalPlugin) GetInfo() iplugin.PluginInfo {
	return instance.GetInfo()
}

// Start main Loop with json of configured devices as parameter
func (p FinalPlugin) Init(devices []*pb.Device) error {
	return instance.Init(devices)
}

// Execute given function if plugin has new device
func (p FinalPlugin) OnAdd(onAdd func(*iplugin.PlDevice) (*pb.Device, error)) {
	instance.OnAdd(onAdd)
}

// Execute given function if plugin has removed device
func (p FinalPlugin) OnRemove(onRemove func(device *pb.Device)) {
	instance.OnRemove(onRemove)
}

// Execute given function if a device has an update
func (p FinalPlugin) OnUpdate(onUpdate func(*pb.Command)) {
	instance.OnUpdate(onUpdate)
}

// Send given state to physical device to update
func (p FinalPlugin) SetState(u *pb.Command) error {
	return instance.SetState(u)
}

// Close connections to devices
func (p FinalPlugin) Close() {
	instance.Close()
}
func GetAutohomePlugin() iplugin.IAutoHomePlugin {
	instance = PioneerPlugin{
		devices: make(map[string]*PioneerDevice)}
	return FinalPlugin{}
}
